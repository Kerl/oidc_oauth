import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Http, RequestOptions, Headers } from '@angular/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  apiResponse;

  constructor(private oAuthService: OAuthService, private http: Http) { }

  ngOnInit() {
  }

  public login() {
    //this.oAuthService.initImplicitFlow();

    this.oAuthService.fetchTokenUsingPasswordFlowAndLoadUserProfile('bob', 'password').then(() => {
      let claims = this.oAuthService.getIdentityClaims();
      if (claims) console.debug(claims);
    });
  }

  public logoff() {
    this.oAuthService.logOut();
  }

  public callApi() {
    let rHeaders = new Headers();
    rHeaders.append('Authorization', 'Bearer ' + this.oAuthService.getAccessToken());

    let options = new RequestOptions({ headers: rHeaders });

    this.http.get('http://localhost:5001/identity', options).map(r => r.json()).subscribe(data => {
      this.apiResponse = JSON.stringify(data);
    });
  }

  public get claims() {
    const claims = this.oAuthService.getIdentityClaims();
    if (!claims) return null;
    return JSON.stringify(claims);
  }
}
